package com.allstate.PaymentSystem;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PaymentSystemApplication {

	private static final Logger LOGGER = LoggerFactory.getLogger(PaymentSystemApplication.class);
	public static void main(String[] args) {

		LOGGER.info("......Spring init");
		SpringApplication.run(PaymentSystemApplication.class, args);
		LOGGER.warn("......Spring loaded");
	}

}

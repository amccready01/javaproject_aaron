package com.allstate.PaymentSystem.dao;

import com.allstate.PaymentSystem.entities.Payment;

import java.util.Collection;
import java.util.List;

public interface IPaymentDAO {

    int rowcount();
    Payment findById(int id);
    List<Payment> findByType(String type);
    int save(Payment payment);
    List<Payment> getAllPayments();

}

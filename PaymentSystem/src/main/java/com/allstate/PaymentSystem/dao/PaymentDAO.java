package com.allstate.PaymentSystem.dao;

import com.allstate.PaymentSystem.entities.Payment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public class PaymentDAO implements IPaymentDAO {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public int rowcount() {
        Query query = new Query();
        return (int)mongoTemplate.count(query, Payment.class);
    }

    @Override
    public Payment findById(int id) {
        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(id));
        Payment payment = mongoTemplate.findOne(query, Payment.class);
        return payment;
    }

    @Override
    public List<Payment> findByType(String type) {
        Query query = new Query();
        query.addCriteria(Criteria.where("type").is(type));
        List<Payment> payment = mongoTemplate.find(query, Payment.class);
        return payment;
    }

    @Override
    public int save(Payment payment) {
        mongoTemplate.insert(payment);
        //TODO: NEED TO REFACTOR TO RETURN 1 OR 0 DEPENDENT ON SUCCESS OR NOT
        return 0;
    }

    @Override
    public List<Payment> getAllPayments() {
        return mongoTemplate.findAll(Payment.class);

    }
}

package com.allstate.PaymentSystem.entities;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.context.annotation.Bean;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class Payment {

    private int id;
//    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
@JsonFormat(pattern="yyyy-MM-dd")
    private Date paymentdate;
    private String type;
    private double amount;
    private int custid;

    public Payment(){}
    public Payment(int id, Date paymentdate, String type, double amount, int custid) {
        this.id = id;
        this.paymentdate = paymentdate;
        this.type = type;
        this.amount = amount;
        this.custid = custid;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getPaymentdate() {
        return paymentdate;
    }

    public void setPaymentdate(Date paymentdate) {
        this.paymentdate = paymentdate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public int getCustid() {
        return custid;
    }

    public void setCustid(int custid) {
        this.custid = custid;
    }

    @Override
    public String toString() {
        return "Payment{" +
                "id=" + id +
                ", paymentdate=" + paymentdate +
                ", type='" + type + '\'' +
                ", amount=" + amount +
                ", custid=" + custid +
                '}';
    }
}

package com.allstate.PaymentSystem.rest;
import com.allstate.PaymentSystem.entities.Payment;
import com.allstate.PaymentSystem.services.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.List;

@CrossOrigin(origins="*")
@RestController
@RequestMapping("/api/payment")
public class PaymentController {

    @Autowired
    PaymentService paymentService;

    @RequestMapping(value = "/status", method = RequestMethod.GET)
    public String status() {
        boolean statusflag = true;
        if (rowcount() == 0) {
            statusflag = false;
        }
        if (findById(1) == null) {
            statusflag = false;
        }

        if (findByType("Standard payment") == null) {
            statusflag  = false;
        }

        if (statusflag) {
            return "Service operational";
        } else {
            return "Service not fully operational";
        }
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public int rowcount(){
        return paymentService.rowcount();
    }

   // @RequestMapping(value = "/all", method = RequestMethod.GET)
   @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity<List>  findAllPayments(){
        List<Payment> payments =  paymentService.findAllPayments();
        if (payments.isEmpty()) {
            return new ResponseEntity<List>(HttpStatus.NOT_FOUND);
        }
        else {
            return new ResponseEntity<List>(payments, HttpStatus.OK);
            //return new ResponseEntity<Collection>(, HttpStatus.OK);
        }
    }

    @RequestMapping(value="id/{id}", method= RequestMethod.GET)
    public ResponseEntity<Payment> findById(@PathVariable("id") int id){
        Payment payment = paymentService.findById(id);
        if (payment == null) {
            return new ResponseEntity<Payment>(HttpStatus.NOT_FOUND);
        }
        else
        {
            return new ResponseEntity<Payment>(payment,HttpStatus.OK);
        }

    }

    @RequestMapping(value="type/{type}", method= RequestMethod.GET)
    public ResponseEntity<List> findByType(@PathVariable("type") String type){
        List<Payment> payments =  paymentService.findByType(type);

        if (payments.isEmpty()) {
            return new ResponseEntity<List>(HttpStatus.NOT_FOUND);
        }
        else {
            return new ResponseEntity<List>(payments, HttpStatus.OK);
            //return new ResponseEntity<Collection>(, HttpStatus.OK);
        }
    }

    @RequestMapping(method = RequestMethod.POST)
    public int save(@RequestBody Payment payment){

        return paymentService.save(payment);
    }


}

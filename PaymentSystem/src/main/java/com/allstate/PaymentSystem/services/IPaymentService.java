package com.allstate.PaymentSystem.services;

import com.allstate.PaymentSystem.entities.Payment;

import java.util.List;

public interface IPaymentService {

    int rowcount();
    Payment findById(int id);
    List<Payment> findByType(String type);
    int save(Payment payment);
    List<Payment> findAllPayments();
}

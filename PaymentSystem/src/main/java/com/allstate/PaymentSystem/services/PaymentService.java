package com.allstate.PaymentSystem.services;

import com.allstate.PaymentSystem.dao.PaymentDAO;
import com.allstate.PaymentSystem.entities.Payment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PaymentService implements  IPaymentService{

    @Autowired
    PaymentDAO paymentDAO;

    @Override
    public int rowcount() {
        return paymentDAO.rowcount();
    }

    @Override
    public Payment findById(int id) {
        if (id > 0) {
            return paymentDAO.findById(id);
        }
        else {
            return null;
        }
    }

    @Override
    public List<Payment> findByType(String type) {
        if (!type.isEmpty()) {
            return paymentDAO.findByType(type);
        }
        else {
            return null;
        }
    }

    @Override
    public List<Payment> findAllPayments() {
            return paymentDAO.getAllPayments();
    }


    @Override
    public int save(Payment payment) {
        return paymentDAO.save(payment);
    }
}

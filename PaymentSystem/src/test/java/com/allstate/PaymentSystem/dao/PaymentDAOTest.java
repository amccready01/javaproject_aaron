package com.allstate.PaymentSystem.dao;

import com.allstate.PaymentSystem.entities.Payment;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

@SpringBootTest
public class PaymentDAOTest {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private PaymentDAO paymentDAO;

    @Autowired
    Payment payment;

    @BeforeEach
    void setUp() {
        payment.setAmount(100.01);
        payment.setCustid(0001);
        payment.setId(0001);
        payment.setType("Standard payment");
        String sDate1="23/09/2020";
        try {
            payment.setPaymentdate(new SimpleDateFormat("dd/MM/yyyy").parse(sDate1));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void canSaveSuccessfully() {
        int callResult = paymentDAO.save(payment);
        Assert.assertEquals(0, callResult);
    }

    @Test
    public void canfindByIdSuccessfully() {
        int callResult = paymentDAO.save(payment);
        Payment payment = paymentDAO.findById(0001);
        Assert.assertNotNull("Check if payment is found by id", payment);
    }

    @Test
    public void canfindByTypeSuccessfully() {
        int callResult = paymentDAO.save(payment);
        List<Payment> payment = paymentDAO.findByType("Standard payment");
        Assert.assertNotNull("Check if payment is found by type", payment);
    }

    @Test
    public void testRowCount() {
        int callResult = paymentDAO.save(payment);
        Assert.assertEquals(1, paymentDAO.rowcount());
    }

    //@AfterEach
    @BeforeEach
    public void cleanUp() {
        for (String collectionName : mongoTemplate.getCollectionNames()) {
            if (!collectionName.startsWith("system.")) {
                mongoTemplate.dropCollection(collectionName);
            }
        }
    }



}

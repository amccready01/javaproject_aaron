package com.allstate.PaymentSystem.entities;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.junit.Assert;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

@SpringBootTest
public class PaymentClassTest {

        @Autowired
        Payment payment;

        @BeforeEach
        void setUp() {
            payment.setAmount(100.01);
            payment.setCustid(0001);
            payment.setId(0001);
            payment.setType("Standard payment");
            String sDate1="23/09/2020";
            try {
                    payment.setPaymentdate(new SimpleDateFormat("dd/MM/yyyy").parse(sDate1));
            } catch (ParseException e) {
                    e.printStackTrace();
            }
        }

        @Test
        public void getPaymentDetails(){

            System.out.println(payment.toString());
            String expected = "Payment{id=1, paymentdate=Wed Sep 23 00:00:00 UTC 2020, type='Standard payment', amount=100.01, custid=1}";
            //my_string.replaceAll("\\p{Cntrl}", "?");
            //    Assert.
            //Assert.assertEquals(expected.replaceAll("\\p{C}", "?"), (String)payment.toString().replaceAll("\\p{C}", "?"));
            Assert.assertEquals(expected, payment.toString());

        }
}

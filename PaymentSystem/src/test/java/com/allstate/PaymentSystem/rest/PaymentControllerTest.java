package com.allstate.PaymentSystem.rest;

//import org.junit.Assert;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.jupiter.api.Assertions.*;

//@RunWith(SpringRunner.class)
@SpringBootTest
public class PaymentControllerTest {

    @Test
    public void statusShouldReturnOperational() throws Exception {
         TestRestTemplate rt = new TestRestTemplate();

        var myresult = rt.getForEntity("http://localhost:8080/api/payment/status", String.class);
        Assert.assertEquals("Service operational", ((ResponseEntity<String>) myresult).getBody());
    }

}
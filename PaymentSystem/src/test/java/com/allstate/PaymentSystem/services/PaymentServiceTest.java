package com.allstate.PaymentSystem.services;

import com.allstate.PaymentSystem.entities.Payment;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@SpringBootTest
public class PaymentServiceTest {


    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private PaymentService paymentService;

    @Autowired
    Payment payment;

    @BeforeEach
    void setUp() {
        payment.setAmount(100.01);
        payment.setCustid(0001);
        payment.setId(0001);
        payment.setType("Standard payment");
        Date sDate1= new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date date = new Date();
        payment.setPaymentdate(date);
    }

    @Test
    public void canSaveSuccessfully(){
        paymentService.save(payment);
        Assert.assertEquals(1, paymentService.rowcount());
    }

    @Test
    public void canfindByTypeSuccessfully(){
        int callResult = paymentService.save(payment);
        List<Payment> payment = paymentService.findByType("Standard payment");
        Assert.assertNotNull("Check if payment is found by type", payment);
    }

    @Test
    public void TestRowCount(){
        int callResult = paymentService.save(payment);
        Assert.assertEquals(1, paymentService.rowcount());
    }

    @BeforeEach
    public void cleanUp() {
        for (String collectionName : mongoTemplate.getCollectionNames()) {
            if (!collectionName.startsWith("system.")) {
                mongoTemplate.dropCollection(collectionName);
            }
        }
    }
}
